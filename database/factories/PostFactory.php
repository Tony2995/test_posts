<?php

use Faker\Generator as Faker;
use Faker\Factory as FakerFactory;

$ips = [];

$faker = FakerFactory::create();

for ($i = 0; $i <= 50; $i++) {
    $ip = $faker->ipv4;
    array_push($ips, $ip);
}


$factory->define(App\Models\Post::class, function (Faker $faker) use ($ips) {
    return [
        'title'      => $faker->streetName,
        'content'    => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'ip'         => $ips[$faker->numberBetween($min = 1, $max = 50)],
        'user_id'    => $faker->numberBetween($min = 1, $max = 100),
        'avg_rating' => 0,
        'login'      => $faker->streetName
    ];
});
