<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(\App\Models\Post::class, 200 * 100)->create()->each(function ($post) {
            $this->command->info('Post: ' . $post->id);
            $post->save();
        });
    }
}
