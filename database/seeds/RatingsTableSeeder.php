<?php

use App\Models\Rating;
use Illuminate\Database\Seeder;

class RatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Rating::class, 1000 * 3)->create()->each(function ($rating) {
            $this->command->info('Rating: ' . $rating->id);
            $rating->save();
        });
    }
}
