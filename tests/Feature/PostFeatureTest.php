<?php

namespace Tests\Feature;

use App\Models\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Factory as FakerFactory;

class PostFeatureTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }


    /** @test */
    public function create_post()
    {
        $post = factory(Post::class)->make();

        $response = $this->json('POST', '/api/create-post', $post->toArray());

        $response
            ->assertStatus(200)
            ->assertJson($post->toArray());
    }

    /** @test */
    public function set_rating()
    {
        $ratingData = [
            'post_id' => 300,
            'value' => 3
        ];

        $post = Post::find(300);

        $response = $this->json('POST', 'api/rating', $ratingData);
        $response
            ->assertStatus(200)
            ->assertJson(['rating' => $post->avg_rating]);
    }


    /** @test */
    public function get_top_count_post()
    {


        $this->assertTrue(true);
//          $posts = factory(Post::class, 2)->create();
//          dd($posts);


//        $faker = FakerFactory::create();
//        $count = $faker->numberBetween($min = 1, $max = 3);

    }
}
