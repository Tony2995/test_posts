<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('create-post', 'EventsController@createPost');
Route::post('rating', 'EventsController@setRating');
Route::get('top-posts', 'EventsController@topPosts');
Route::get('ip-list', 'EventsController@IpAddressList');