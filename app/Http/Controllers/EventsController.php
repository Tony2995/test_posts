<?php

namespace App\Http\Controllers;

use App\Jobs\PostRating;
use App\Models\IpAddress;
use App\Models\Rating;
use App\Models\User;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventsController extends Controller
{
    public function createPost(Request $request)
    {

        $data = $request->all();

        $validator = Validator::make($data, Post::postRules());

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'msg' => $validator->errors(),
            ], 422);
        } else {

            $login = $data['login'];

            $author = User::where('login', $login)->first();


            if (!empty($author)) {
                $this->savePost($data, $author);
            } else {
                $author = User::create([
                    'login' => $data['login']
                ]);



                $this->savePost($data, $author);
            }

            return response()->json($data, 200);
        }
    }

    public function setRating(Request $request)
    {
        $data = $request->all();

        $validator = Validator::make($data, Rating::rulesRating());


        if ($data['value'] > 5) {
            return response()->json([
                'msg' => 'Max value: 5'
            ], 422);
        }

        if ($validator->fails()) {
            return response()->json([
                'msg' => $validator->errors()
            ], 422);
        } else {
            $post_id = $data['post_id'];
            $value = $data['value'];

            $post = Post::find($post_id);


            if (empty($post)) {
                return response()->json([
                    'msg' => 'Post not found'
                ], 404);
            } else {

                PostRating::dispatch($post, $value)->onQueue('post_rating');

                return response()->json([
                    'rating' => $post->avg_rating
                ], 200);

            }

        }
    }

    public function topPosts(Request $request)
    {
        $count = $request->count ? $request->count : null;

        if (!is_null($count)) {
            $posts = Post::select('title', 'content')->orderBy('avg_rating', SORT_DESC)->limit($count)->get();
            return response()->json([$posts]);
        } else {
            return response()->json(['msg' => 'Count not set']);
        }
    }


    public function IpAddressList()
    {
        $ips = IpAddress::all();

        $data = [];

        foreach ($ips as $ip) {

            $data[] = [
                'ip' => $ip->ip,
                'authors' => $ip->users()->pluck('login')
            ];
        }

        return response()->json($data);
    }

    private function savePost($data, $author) {

        Post::create([
            'title' => $data['title'],
            'content' => $data['content'],
            'user_id' => $author->id,
            'ip' => $data['ip']
        ]);

        $ipAddressCurrent = IpAddress::where('ip', $data['ip'])->first();

        $ipAddress = !empty($ipAddressCurrent)
            ?
            $ipAddressCurrent
            :
            IpAddress::create([ 'ip' => $data['ip']]);

        $author->ipAddresses()->save($ipAddress);

    }

}
