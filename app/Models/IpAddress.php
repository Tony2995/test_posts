<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IpAddress extends Model
{

    protected $fillable = ['ip'];

    public function users()
    {
        return $this->belongsTomany(User::class);
    }
}
