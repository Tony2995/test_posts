<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $fillable = [
        'value',
        'post_id'
    ];


    public static function rulesRating()
    {
        return [
            'value' => 'required|numeric',
            'post_id' => 'required|numeric'
        ];
    }
}
