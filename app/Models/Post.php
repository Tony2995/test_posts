<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title',
        'content',
        'ip',
        'user_id'
    ];

    public static function postRules()
    {
        return [
            'title'   => 'required',
            'content' => 'required',
            'login'   => 'required',
            'ip'      => 'required|ip'
        ];
    }


    public function rating()
    {
        return $this->hasMany(Rating::class);
    }
}
