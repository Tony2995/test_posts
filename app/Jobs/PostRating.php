<?php

namespace App\Jobs;

use App\Models\Rating;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class PostRating implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    private $post;
    private $value;


    public function __construct($post, $value)
    {
        $this->post = $post;
        $this->value = $value;
    }

    /**
     * Execute the job.
     *
     * @return void
     */

    public function handle()
    {
        Rating::create([
            'post_id' => $this->post->id,
            'value' =>  $this->value
        ]);

        $this->post->avg_rating = $this->averageRating();
        $this->post->save();

    }

    private function averageRating() {
        $rating = $this->post->rating;
        return $rating->avg('value');
    }
}
